# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

emilui = Usuario.create(nombre: 'Emilio', apellido: 'Uribe', nacimiento: '01/01/1949', email: 'emilio@uribe',
                        roles: 0)
manuel = Usuario.create(nombre: 'Manuel', apellido: 'Uribe', nacimiento: '01/01/1969', email: 'manuel@uribe',
                        roles: 0)
silvia = Usuario.create(nombre: 'Silvia', apellido: 'Uribe', nacimiento: '01/01/1981', email: 'silvia@uribe',
                        roles: 1)
quantum = Usuario.create(nombre: 'Quantum', apellido: 'Uribe', nacimiento: '01/01/1994', email: 'quantum@uribe',
                         roles: 2)
carlos = Usuario.create(nombre: 'Carlos', apellido: 'Uribe', nacimiento: '01/01/2010', email: 'carlos@uribe',
                        roles: 2)
celia = Usuario.create(nombre: 'Celia', apellido: 'Uribe', nacimiento: '01/01/1993', email: 'celia@uribe',
                       roles: 2)
milly = Usuario.create(nombre: 'Milly', apellido: 'Uribe', nacimiento: '01/01/1999', email: 'millenial@premium',
                       roles: 1)

post_1 = Post.create(titulo: 'Un mono', contenido: 'Así es, un mono jaja', usuario_id: emilui)
post_2 = Post.create(titulo: 'OOohoho otro monooo', contenido: 'Así es OTRO MONO jeje', usuario_id: manuel)
post_3 = Post.create(titulo: 'Un monito uwu', contenido: 'Un pequeño monito muy tierno', usuario_id: silvia)
post_4 = Post.create(titulo: "Monon't", contenido: 'Esto si que no es un mono', usuario_id: quantum)
post_5 = Post.create(titulo: 'Uueueheh', contenido: 'JIjiij el 5', usuario_id: carlos)
post_6 = Post.create(titulo: 'Juju', contenido: 'Cuidado con el voodoo', usuario_id: celia)
post_7 = Post.create(titulo: 'Número, 7', contenido: 'Mi libro, luna de Plutón', usuario_id: emilui)
post_8 = Post.create(titulo: 'Oooocho', contenido: 'Quinii quiniii', usuario_id: emilui)
post_9 = Post.create(titulo: 'Un monito', contenido: 'Otro monito muy tierno', usuario_id: milly)
