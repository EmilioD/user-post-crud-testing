# frozen_string_literal: true

class FixRolesToUsuarios < ActiveRecord::Migration[7.0]
  def change
    # remove column roles from usuarios and add column roles to usuarios as integer
    remove_column :usuarios, :roles
    add_column :usuarios, :roles, :integer
  end
end
