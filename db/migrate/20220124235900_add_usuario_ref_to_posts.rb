# frozen_string_literal: true

class AddUsuarioRefToPosts < ActiveRecord::Migration[7.0]
  def change
    add_reference :posts, :usuario, null: false, foreign_key: true
  end
end
