# frozen_string_literal: true

class RemoveRolesFromUsuarios < ActiveRecord::Migration[7.0]
  def change
    remove_column :usuarios, :roles, :string
  end
end
