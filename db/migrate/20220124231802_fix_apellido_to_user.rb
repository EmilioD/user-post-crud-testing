# frozen_string_literal: true

class FixApellidoToUser < ActiveRecord::Migration[7.0]
  def change
    rename_column :usuarios, :apellidos, :apellido
  end
end
