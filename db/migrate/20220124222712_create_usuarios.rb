# frozen_string_literal: true

class CreateUsuarios < ActiveRecord::Migration[7.0]
  def change
    create_table :usuarios do |t|
      t.string :nombre
      t.string :apelido
      t.string :email
      t.date :nacimiento
      t.string :roles

      t.timestamps
    end
  end
end
