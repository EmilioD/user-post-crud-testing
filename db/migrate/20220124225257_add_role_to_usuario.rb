# frozen_string_literal: true

class AddRoleToUsuario < ActiveRecord::Migration[7.0]
  def change
    add_column :usuarios, :roles, :jsonb, null: false, default: {}
    add_index :usuarios, :roles, using: :gin
  end
end
