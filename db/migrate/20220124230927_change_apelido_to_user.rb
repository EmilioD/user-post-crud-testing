# frozen_string_literal: true

class ChangeApelidoToUser < ActiveRecord::Migration[7.0]
  def change
    rename_column :usuarios, :apellidos, :apellido
  end
end
