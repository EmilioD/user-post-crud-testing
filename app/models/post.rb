# frozen_string_literal: true

class Post < ApplicationRecord
  # Relaciones
  belongs_to :usuario

  # Validaciones
  validates :titulo, :contenido, presence: true
  validates :titulo, length: { maximum: 30 }
  validates :contenido, presence: true

  # Scopes
  # A scope that gives the posts of the last 5 days
  scope :hace_5_dias, -> { where('created_at >= ?', 5.days.ago) && order(created_at: :desc) }

  scope :recientes, -> { order(created_at: :desc).limit(3) }

  scope :not_millenials, -> { where.not(usuario_id: Usuario.millenials) }

  scope :premiums_millenials, -> { where(usuario_id: Usuario.millenials && Usuario.premiums) }
end
