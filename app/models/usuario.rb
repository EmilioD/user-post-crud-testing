# frozen_string_literal: true

class Usuario < ApplicationRecord
  # Relaciones
  has_many :posts

  enum roles: %i[admin premium regular]

  # Validaciones
  validates :nombre, presence: true
  validates :apellido, presence: true
  validates :email, presence: true, uniqueness: true
  validates :nacimiento, presence: true
  validates :roles, presence: true

  # Scopes
  scope :centennials, -> { where(nacimiento: Date.new(1994)..Date.new(2010)) }
  scope :millenials, ->  { where(nacimiento: Date.new(1981)..Date.new(1993)) }
  scope :xgeneration, -> { where(nacimiento: Date.new(1970)..Date.new(1980)) }
  scope :boomers, ->     { where(nacimiento: Date.new(1949)..Date.new(1969)) }
  scope :premiums, -> { where(roles: 'premium') }
end
