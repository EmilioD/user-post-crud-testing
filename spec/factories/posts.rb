# frozen_string_literal: true

FactoryBot.define do
  factory :story do
    titulo { Faker::Lorem.title }
    contenido { Faker::Name.sentence }
  end
end
