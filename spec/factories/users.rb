# frozen_string_literal: true

FactoryBot.define do
  factory :usuario do
    nombre { Faker::Name.first_name }
    apellido { Faker::Name.last_name }
    nacimiento { Faker::Date.birthday(min_age: 18, max_age: 65) }
    roles { %i[admin premium regular].sample }
    email { Faker::Internet.email }

    factory :user_with_posts do
      after :create do |usuario|
        create_list(:post, 3, user: usuario)
      end
    end
  end
end
