# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'UsuariosController', type: :request do
  describe 'GET index' do
    before { get usuarios_path }

    context 'when request is valid' do
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:index) }
    end
  end

  describe 'GET show' do
    let!(:usuario) { create(:usuario) }

    before { get usuario_path(usuario) }

    context 'when request is valid' do
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:show) }
    end
  end

  describe 'GET new' do
    before { get new_usuario_path }

    context 'when request is valid' do
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to render_template(:new) }
    end
  end

  describe 'POST create' do
    it do
      params = {
        usuario: {
          nombre: 'John',
          apellido: 'Doe',
          email: 'johndoe@example.com',
          nacimiento: '1990-01-01',
          roles: 'regular'
        }
      }
      should permit(:nombre, :apellido, :email, :nacimiento, :roles).for(:create, params: params).on(:usuario)
    end

    context 'when request is valid' do
      it { expect(response).to have_http_status(:ok)}
      it { expect(response).to redirect_to(usuarios_path) }
    end
  end

  describe 'GET edit' do; end
  describe 'PUT update' do; end


  describe 'DELETE destroy' do
    before { delete :destroy }
    
    context 'when request is valid' do
      it { expect(response).to have_http_status(:ok) }
      it { expect(response).to redirect_to(:usuarios_url) }
    end
  end
  describe 'GET download' do; end
end
