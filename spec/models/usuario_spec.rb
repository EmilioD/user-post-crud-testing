# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Usuario, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:nombre) }
    it { should validate_presence_of(:apellido) }
    it { should validate_presence_of(:nacimiento) }
    it { should validate_presence_of(:roles) }
  end

  describe 'uniqueness validations' do
    subject { create(:usuario) }

    it { should validate_uniqueness_of(:email) }
  end

  describe 'associations' do
    it { should have_many(:posts) }
  end

  describe 'scopes' do
    describe 'centennials' do
      subject { create(:usuario, nacimiento: Date.new(1994)..Date.new(2010)) }
    end
    describe 'millenials' do
      subject { create(:usuario, nacimiento: Date.new(1981)..Date.new(1993)) }
    end
    describe 'xgeneration' do
      subject { create(:usuario, nacimiento: Date.new(1970)..Date.new(1980)) }
    end
    describe 'boomers' do
      subject { create(:usuario, nacimiento: Date.new(1949)..Date.new(1969)) }
    end
    describe 'premiums' do
      subject { create(:usuario, roles: 'premium') }
    end
  end

  describe 'Enumeratives' do
    it { should define_enum_for(:roles).with_values(%i[admin premium regular]) }
  end
end
