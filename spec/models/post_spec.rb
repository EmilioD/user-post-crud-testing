# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'associations' do
    it { should belong_to :usuario }
  end

  describe 'validations' do
    it { should validate_presence_of(:titulo) }
    it { should validate_length_of(:titulo).is_at_most(30) }
  end

  describe 'Scopes' do; end
end
