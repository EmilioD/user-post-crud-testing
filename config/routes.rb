# frozen_string_literal: true

Rails.application.routes.draw do
  resources :posts
  resources :usuarios
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'usuarios#index'
end
